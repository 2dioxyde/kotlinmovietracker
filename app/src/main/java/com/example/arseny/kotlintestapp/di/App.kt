package com.example.arseny.kotlintestapp.di

import android.app.Application

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
//        lateinit var authorizationComponent: AuthorizationComponent
//        lateinit var dataAccessComponent: DataAccessComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().build()
//        authorizationComponent = DaggerAuthorizationComponent.builder().build()
    }
}
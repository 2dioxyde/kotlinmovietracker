package com.example.arseny.kotlintestapp.utils

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class LoadMoreListener(var layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemsCount = layoutManager.childCount
        val totalItemsCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        if (!isLoading() && !isLastPage()) {
            if (firstVisibleItemPosition + visibleItemsCount >= totalItemsCount && firstVisibleItemPosition >= 0) {
                loadMorItems()
            }
        }
    }

    abstract fun loadMorItems()
}
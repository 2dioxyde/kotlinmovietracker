package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.data.DataAccessLayer
import dagger.Component
import javax.inject.Singleton

@Singleton
//@Component(modules = [DataAccessModule::class])
interface DataAccessComponent {

    fun dataAccessLayer(): DataAccessLayer
}
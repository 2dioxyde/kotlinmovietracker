package com.example.arseny.kotlintestapp.model

import com.google.gson.annotations.SerializedName

data class ResultsModel(

        @SerializedName("page")
        var page: Int,

        @SerializedName("total_results")
        var resultsCount: Int,

        @SerializedName("total_pages")
        var pages: Int,

        @SerializedName("results")
        var results: MutableList<MovieModel>

//{
// "page":1,
// "total_results":649,
// "total_pages":33,
// "results":[{}]
// }
)
package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.data.AuthorizationLayer
import dagger.Component
import javax.inject.Singleton

@Singleton
//@Component(modules = [AuthorizationModule::class])
interface AuthorizationComponent {

    fun authorizationComponent(): AuthorizationLayer
}
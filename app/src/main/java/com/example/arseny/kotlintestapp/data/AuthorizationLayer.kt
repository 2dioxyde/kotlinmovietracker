package com.example.arseny.kotlintestapp.data

import android.support.v4.app.Fragment
import com.example.arseny.kotlintestapp.view.AccountFragment
import com.example.arseny.kotlintestapp.view.LoginFragment
import io.reactivex.subjects.BehaviorSubject

class AuthorizationLayer {

    private var o = 0
    val authSubject: BehaviorSubject<Fragment> = BehaviorSubject.create()

    fun login() {
        authSubject.onNext(AccountFragment())
    }

    fun logout() {
        authSubject.onNext(LoginFragment())
    }
}
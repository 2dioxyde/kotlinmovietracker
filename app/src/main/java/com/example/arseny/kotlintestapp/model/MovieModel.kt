package com.example.arseny.kotlintestapp.model

import com.google.gson.annotations.SerializedName

data class MovieModel(

        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val title: String,
        @SerializedName("original_title")
        val originalTitle: String,
        @SerializedName("poster_path")
        var posterPath: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("release_date")
        val releaseDate: String
)
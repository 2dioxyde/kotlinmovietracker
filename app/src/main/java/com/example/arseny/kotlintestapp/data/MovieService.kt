package com.example.arseny.kotlintestapp.data

import com.example.arseny.kotlintestapp.model.ResultsModel
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.*

interface MovieService {

    @GET("search/movie/")
    fun getMovie(@Query("api_key") apiKey: String,
                 @Query("query") query: String,
                 @Query("page") page: Int): Observable<ResultsModel>

    @GET("authentication/token/new")
    fun requestToken(@Query("api_key") apiKey: String): Observable<JsonObject>

    @FormUrlEncoded
    @POST("authentication/token/validate_with_login")
    fun login(@Query("api_key") apiKey: String,
              @Field("username") login: String,
              @Field("password") password: String,
              @Field("request_token") token: String): Observable<JsonObject>

    @DELETE("authentication/session")
    fun logout(@Query("api_key") apiKey: String,
               @Query("session_id") sessionId: String)

    @GET()
    fun approveToken(@Url() url: String,
                     @Path("REQUEST_TOKEN") token: String)

    @GET("account")
    fun account(@Query("api_key") apiKey: String,
                @Query("sesion_id") sessionId: String): Observable<JsonObject>

    @GET("/account/{account_id}/lists")
    fun getCreatedLists(@Path("account_id") accountId: Int,
                        @Query("api_key") apiKey: String,
                        @Query("session_id") sessionId: String
    )
}

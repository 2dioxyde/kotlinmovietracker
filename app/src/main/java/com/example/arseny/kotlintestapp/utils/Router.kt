package com.example.arseny.kotlintestapp.utils

import com.example.arseny.kotlintestapp.di.App

class Router {

    init {
        App.appComponent.inject(this)
    }

    fun navigateToAccount(): Boolean {
        return false
    }
}
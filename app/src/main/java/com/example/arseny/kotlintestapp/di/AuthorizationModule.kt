package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.data.AuthorizationLayer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AuthorizationModule {

    @Provides
    @Singleton
    fun provideAuthorizationLayer(): AuthorizationLayer = AuthorizationLayer()
}
package com.example.arseny.kotlintestapp.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.arseny.kotlintestapp.R
import com.example.arseny.kotlintestapp.model.MovieModel
import kotlinx.android.synthetic.main.main_list_item.view.*

class MainListAdapter(val context: Context) : RecyclerView.Adapter<MovieViewHolder>() {

    private var dataSet: MutableList<MovieModel> = ArrayList()

    override fun onCreateViewHolder(group: ViewGroup, p1: Int): MovieViewHolder {
        return MovieViewHolder(LayoutInflater.from(context).inflate(R.layout.main_list_item, group, false))
    }

    override fun getItemCount(): Int {
        return dataSet.count()
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val model = dataSet.get(position)
        holder.title.text = model.title
        holder.overview.text = model.overview
        holder.date.text = model.releaseDate
        Glide.with(context)
                .load(model.posterPath)
                .apply(RequestOptions().placeholder(R.drawable.placeholder))
                .into(holder.poster)
    }

    fun setDataSet(dataSet: MutableList<MovieModel>) {
        this.dataSet = dataSet
    }
}

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val title: TextView = itemView.title
    val overview: TextView = itemView.overview
    val date: TextView = itemView.date
    val poster: ImageView = itemView.poster
}

package com.example.arseny.kotlintestapp.model

import com.google.gson.annotations.SerializedName

class TokenModel(

        @SerializedName("success")
        var success: Boolean,

        @SerializedName("expires_at")
        var expires: String,

        @SerializedName("request_token")
        var token: String
)

package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.utils.Router
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RouterModule {

    @Singleton
    @Provides
    fun provideRouter() = Router()
}
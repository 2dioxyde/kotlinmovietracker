package com.example.arseny.kotlintestapp.main

import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.example.arseny.kotlintestapp.R
import com.example.arseny.kotlintestapp.di.App
import com.example.arseny.kotlintestapp.utils.Router
import com.example.arseny.kotlintestapp.view.MainPagerAdapter
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var router: Router

    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val appComponent = App.appComponent

        appComponent.inject(this)

        RxJavaPlugins.setErrorHandler { t: Throwable? ->
            Toast.makeText(applicationContext, t?.message, android.widget.Toast.LENGTH_SHORT).show()
        }

        setupTabs()
        setupMenu()
    }

    private fun setupTabs() {
        val pagerAdapter = MainPagerAdapter(supportFragmentManager)
        view_pager.adapter = pagerAdapter
        tab_layout.setupWithViewPager(view_pager)
    }

    private fun setupMenu() {
        actionBarDrawerToggle = ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close)
        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        navigation_view.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                1 -> router.navigateToAccount()
                else -> true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

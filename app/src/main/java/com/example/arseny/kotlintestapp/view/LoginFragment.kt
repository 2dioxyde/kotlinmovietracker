package com.example.arseny.kotlintestapp.view

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.arseny.kotlintestapp.R
import com.example.arseny.kotlintestapp.data.AuthorizationLayer
import com.example.arseny.kotlintestapp.data.DataAccessLayer
import com.example.arseny.kotlintestapp.di.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.login_layout.*
import javax.inject.Inject

class LoginFragment : Fragment() {

    @Inject
    lateinit var dataAccessLayer: DataAccessLayer
    @Inject
    lateinit var authorizationLayer: AuthorizationLayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
//        (activity as AppCompatActivity).supportActionBar?.hide()
        login_button.setOnClickListener {

            val loginIsValid = login_text.isNotBlank("Login shouldn't be blank")
            if (password_text.isNotBlank("Password shouldn't be blank") && loginIsValid) {
                try {
                    dataAccessLayer
                            .login(login_text.text.toString(), password_text.text.toString())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { s ->
                                if (!s) {
                                    password_text_layout.error = "Incorrect login or password"
                                } else {
                                    Toast.makeText(context, "You are logged in successfully", android.widget.Toast.LENGTH_SHORT).show()
                                    authorizationLayer.login()
                                }
                            }

                } catch (e: Exception) {
                    Toast.makeText(context, e.message, android.widget.Toast.LENGTH_SHORT).show()
                }
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    private fun TextInputEditText.isNotBlank(message: String): Boolean {
        val parent = this.parent.parent as TextInputLayout
        parent.error = if (this.text.isNullOrBlank()) message else null
        return this.error == null
    }
}
package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.data.DataAccessLayer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataAccessModule {

    @Provides
    @Singleton
    fun provideDataAccessLayer(): DataAccessLayer = DataAccessLayer()
}
package com.example.arseny.kotlintestapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import com.example.arseny.kotlintestapp.R
import com.example.arseny.kotlintestapp.data.DataAccessLayer
import com.example.arseny.kotlintestapp.di.App
import com.example.arseny.kotlintestapp.model.ResultsModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.search_list_layout.*
import javax.inject.Inject

class SearchListFragment : Fragment() {

    @Inject
    lateinit var dataSource: DataAccessLayer

    private lateinit var adapter: MainListAdapter
    private var isLoading = false
    private lateinit var searchQuery: String
    private var data: ResultsModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.appComponent.inject(this)

        adapter = MainListAdapter(context!!)
        val layoutManager = LinearLayoutManager(context)
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = adapter

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    searchQuery = query
                    data = null
                    search(searchQuery)
                    search_view.hideKeyboard()
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                print(newText)
                return true
            }
        })

        recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)

                val visibleIntemsCount = layoutManager.childCount
                val totalIntemsCount = layoutManager.itemCount
                val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

                if (firstVisibleItem + visibleIntemsCount >= totalIntemsCount && !isLoading) {
                    if (data!!.page < data!!.pages) {
                        search(searchQuery, data!!.page + 1)
                        isLoading = true
                    }
                }
            }
        })

        search_view.setOnTouchListener { _, _ ->
            search_view.isIconified = false
            false
        }

        recycler_view.setOnTouchListener { v, _ ->
            v?.hideKeyboard()
            false
        }
    }

    private fun search(text: String) {
        search(text, 1)
    }

    @SuppressLint("CheckResult")
    fun search(text: String, page: Int) {
        dataSource.getMovie(text, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    setData(s)
                },
                        { e -> Toast.makeText(context, e.localizedMessage, android.widget.Toast.LENGTH_SHORT).show() })
    }

    private fun setData(data: ResultsModel) {

        if (this.data != null) {
            data.results.addAll(0, this.data!!.results)
        }
        this.data = data

        adapter.setDataSet(this.data!!.results)
        adapter.notifyDataSetChanged()
        isLoading = false
    }

    fun View.hideKeyboard() {
        val temp = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        temp.hideSoftInputFromWindow(windowToken, 0)
    }
}
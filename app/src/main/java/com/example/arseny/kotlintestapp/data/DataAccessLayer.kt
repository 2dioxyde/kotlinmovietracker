package com.example.arseny.kotlintestapp.data

import android.annotation.SuppressLint
import com.example.arseny.kotlintestapp.model.ResultsModel
import com.example.arseny.kotlintestapp.model.TokenModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.ReplaySubject
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class DataAccessLayer {

    private val apiKey = "a093717a0800c7d91fe10fee8585ff0e"
    private val baseUrl = "https://api.themoviedb.org/3/"
    val imgUrl = "https://image.tmdb.org/t/p/w200_and_h300_bestv2/"
    val userApprovalUrl = "https://www.themoviedb.org/authenticate/{REQUEST_TOKEN}/allow/"
    private lateinit var sessionToken: String
    private lateinit var approvedToken: String

    private val client = OkHttpClient.Builder().build()

    private val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

    private val movieService = retrofit.create(MovieService::class.java)

    @SuppressLint("CheckResult")
    fun getMovie(query: String, page: Int): BehaviorSubject<ResultsModel> {

        val result: BehaviorSubject<ResultsModel> = BehaviorSubject.create()

        movieService.getMovie(apiKey, query, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ v ->
                    run {
                        v.results.forEach { movieModel ->
                            kotlin.run {
                                movieModel.posterPath = imgUrl + movieModel.posterPath
                            }
                        }
                        result.onNext(v)
                    }
                },
                        { e -> print(e) })

        return result
    }

    @SuppressLint("CheckResult")
    private fun requestToken(): ReplaySubject<Boolean> {

        val result = ReplaySubject.create<Boolean>()
        movieService.requestToken(apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onNext ->
                    run {
                        val tokenModel = Gson().fromJson<TokenModel>(onNext, TokenModel::class.java)
                        if (tokenModel.success) {
                            sessionToken = tokenModel.token
                            result.onNext(true)
                        }
                    }
                }
        return result
    }

    @SuppressLint("CheckResult")
    fun login(login: String, password: String): BehaviorSubject<Boolean> {
        val result = BehaviorSubject.create<Boolean>()
        requestToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    approveToken(login, password)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { s -> result.onNext(s) }
                }
        return result
    }

    @SuppressLint("CheckResult")
    fun approveToken(login: String, password: String): BehaviorSubject<Boolean> {
        val result = BehaviorSubject.create<Boolean>()
        movieService.login(apiKey, login, password, sessionToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    run {
                        val tokenModel = Gson().fromJson<TokenModel>(s, TokenModel::class.java)
                        if (tokenModel.success) {
                            approvedToken = tokenModel.token
                            result.onNext(true)
                        }
                    }
                }, { _ ->
                    result.onNext(false)
                })
        return result
    }
}

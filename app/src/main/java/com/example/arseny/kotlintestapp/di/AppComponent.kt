package com.example.arseny.kotlintestapp.di

import com.example.arseny.kotlintestapp.main.MainActivity
import com.example.arseny.kotlintestapp.utils.Router
import com.example.arseny.kotlintestapp.view.LoginFragment
import com.example.arseny.kotlintestapp.view.MainPagerAdapter
import com.example.arseny.kotlintestapp.view.SearchListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AuthorizationModule::class, DataAccessModule::class, RouterModule::class])
interface AppComponent {



    fun inject(target: MainActivity)

    fun inject(target: MainPagerAdapter)

    fun inject(target: LoginFragment)

    fun inject(target: SearchListFragment)

    fun inject(target: Router)
}